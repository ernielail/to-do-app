//
//  SecondViewController.swift
//  To Do List App
//
//  Created by Ernie Lail on 12/31/17.
//  Copyright © 2017 Ernie Lail. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITextFieldDelegate {

    
    var items = [AnyObject!]()

    @IBOutlet var itemName: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func addItem(_ sender: Any) {
        let savedData = UserDefaults.standard.object(forKey: "array4")
        if (savedData) != nil{
            items = (savedData as! [AnyObject?])
            let toInsert = itemName.text
            items.append(toInsert! as AnyObject)
        }
        else{
            print("Could not define array from save, assigning a new one")
            let toInsert = itemName.text
            items.append(toInsert! as AnyObject)
        }
        print("New Item Added")
        print("New Array Count \(items.count)")
        UserDefaults.standard.set(items, forKey: "array4")
        UserDefaults.standard.synchronize()
        itemName.text = ""
    }
    
    

}

