//
//  FirstViewController.swift
//  To Do List App
//
//  Created by Ernie Lail on 12/31/17.
//  Copyright © 2017 Ernie Lail. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var table: UITableView!
    
    var items = [AnyObject!]()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        let savedData = UserDefaults.standard.object(forKey: "array4")
        if (savedData) != nil{
            items = (savedData as! [AnyObject?])
        }
        table.reloadData();
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            print("Removing Item: \(items[indexPath.row])")
            let removeMe = indexPath.row
            items.remove(at: removeMe)
            UserDefaults.standard.set(items, forKey: "array4")
            table.reloadData()
        }
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return (items.count)
    }
    
    
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
        cell.textLabel?.text = (items[indexPath.row] as! String)
        return cell
    }
    
    
    
    
}

